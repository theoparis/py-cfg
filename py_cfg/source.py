import os
from abc import ABC, abstractmethod
from typing import Any, Optional, Union
from anyser import dumps, loads
import dotted


class ConfigSource(ABC):
    @abstractmethod
    def get_config_value(self, key_name: str) -> Optional[str]:
        raise NotImplementedError()

    @abstractmethod
    def set_config_value(self, key_name: str, value: str) -> Optional[str]:
        raise NotImplementedError()


class AbstractDictConfigSource(ConfigSource):
    _config: dict

    def __init__(self, config: dict):
        self._config = config

    def get_config_value(self, key_name: str) -> Optional[Union[str, int, float, None]]:
        try:
            return dotted.get(self._config, key_name)
        except:
            return None

    def set_config_value(
        self, key_name: str, value: Union[str, int, float, None]
    ) -> Optional[Union[str, int, float, None]]:
        self._config = dotted.update(self._config, key_name, str(value))
        try:
            return dotted.get(self._config)
        except:
            return None


class AnyserFileConfigSource(AbstractDictConfigSource):
    def __init__(self, filename: str, fileFormat: str = "yaml", must_exist=True):
        super().__init__({})
        self.filename = filename
        self.fileFormat = fileFormat
        self.must_exit = must_exist
        self.read()

    def read(self):
        if os.path.exists(self.filename):
            with open(self.filename, "r") as file:
                self._config.update(loads(file.read(), self.fileFormat))
        elif self.must_exist:
            raise FileNotFoundError(f"Could not find config file {self.filename}")

    def write(self):
        if os.path.exists(self.filename):
            with open(self.filename, "w") as file:
                file.write(dumps(self._config.to_dict(), self.fileFormat))
        elif self.must_exist:
            raise FileNotFoundError(f"Could not find config file {self.filename}")

    def set_config_value(self, key_name: str, value: str) -> Optional[str]:
        self.write()
        return super().set_config_value(key_name=key_name, value=value)

    def get_config_value(self, key_name: str) -> Optional[str]:
        self.read()
        return super().get_config_value(key_name)
